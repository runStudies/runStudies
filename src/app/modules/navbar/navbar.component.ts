import { Component, OnInit } from '@angular/core';

import { ModalLoginComponent } from "../component/modal-login/modal-login.component";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  providers: [ModalLoginComponent]
})
export class NavbarComponent implements OnInit {

  clickMessage = '';

  constructor(
    public modalLogin: ModalLoginComponent
  ) { }

  ngOnInit() { }

  openModal() {
    console.log(this.clickMessage = 'Hello Word');
    this.modalLogin.openModal();
  }
}