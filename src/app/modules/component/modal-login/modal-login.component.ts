import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss'],
  exportAs: 'modalLogin'
})
export class ModalLoginComponent {
  @ViewChild('modalLogin') public modalLogin: ModalDirective;
  public name: string;

  constructor() { }

  openModal(): void {
    this.name = 'Hello Word';
    this.modalLogin.show();
  }

}
