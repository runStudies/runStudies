import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousels',
  templateUrl: './carousels.component.html',
  styleUrls: ['./carousels.component.scss']
})
export class CarouselsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public myInterval: number = 3000;
  public activeSlideIndex: number = 0;
  public noWrapSlides: boolean = false;

  activeSlideChange() {
    console.log(this.activeSlideIndex);
  }

  public slides: Array<Object> = [
    { "image": "assets/carrousel.jpg" },
    { "image": "assets/carrousel.jpg" },
    { "image": "assets/carrousel.jpg" },
  ];

}
